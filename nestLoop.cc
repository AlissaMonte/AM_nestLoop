#include <iostream>
#include <string>
#include <fstream>

#include "TFile.h"
#include "TTree.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TGraph.h"
#include "NEST.hh"
#include "LZDetector.hh"

int main(int argc, char *argv[])
{
  double density = 2.888; //g/cm^3 for LXe
  double nest_A = 0;
  double nest_Z = 0; //required for ion calculations (we're doing gamma, so set them to zero...)
  double energy = 10; //keV = energy deposited by our Co57 gamma
  LZDetector* fDetector = new LZDetector();

  double fieldLo = 0.;
  double fieldHi = 10000.;
  double fieldStep = 100.;

  std::cout << "\n\n====================================\n" << std::endl;
  std::cout << "Running NEST for " << energy << " keV gamma." << std::endl;
  std::cout << "Calculating yield as a funtion of field from " << fieldLo << " V/cm to " << fieldHi << " V/cm in " << fieldStep << " V/cm increments..." << std::endl;

  std::cout << "Yield (raw S1 photons): { ";
  NEST::NESTcalc* fNestCalc = new NEST::NESTcalc(fDetector);

  for(double Vcm = fieldLo; Vcm < fieldHi; Vcm+=fieldStep) { //loop over field values

    NEST::YieldResult nestYields =
      fNestCalc->GetYields(NEST::gammaRay, energy, density,
			   Vcm, nest_A, nest_Z, {1., 1.});
    NEST::QuantaResult  nestQuanta = fNestCalc->GetQuanta(nestYields, density);
    //    double noOfPhotons = nestQuanta.photons;
    double nAvgPhotons = nestYields.PhotonYield;
    std::cout << nAvgPhotons << ", "; //this is the number of raw S1 photons we expect to appear
  }

  delete fNestCalc;
  std::cout << "}" << std::endl;
  std::cout << "\n====================================\n" << std::endl;

}





