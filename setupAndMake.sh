#!/usr/bin/env bash

LZ_GEANT_VERSION="${LZ_GEANT_VERSION:-10.03.p02}"
LZ_NEST_VERSION="${LZ_NEST_VERSION:-2.0.1_d}"
LZBUILD_VERSION="${LZBUILD_VERSION:-3.0.3}"

export LZ_GEANT_VERSION
export LZBUILD_VERSION

#NEST_DIR=/cvmfs/lz.opensciencegrid.org/external/NEST/2.0.1_G4.10.03.p02/x86_64-centos7-gcc7-opt/
#NEST_DIR=/cvmfs/lz.opensciencegrid.org/external/NEST/latest/x86_64-centos7-gcc7-opt
#LZNESTUTILS_DIR=/cvmfs/lz.opensciencegrid.org/LZNESTUtils/latest/x86_64-centos7-gcc7-opt
#LZBUILD_DIR=/cvmfs/lz.opensciencegrid.org/LzBuild/release-${LZBUILD_VERSION}
LZBUILD_DIR=/cvmfs/lz.opensciencegrid.org/LzBuild/latest    

source ${LZBUILD_DIR}/setup.sh
NEST_DIR=/cvmfs/lz.opensciencegrid.org/external/NEST/latest/x86_64-centos7-gcc7-opt

g++ nestLoop.cc `root-config --cflags --libs` -I${NEST_DIR}/include -L${NEST_DIR}/lib -lNEST -o nestLoop
